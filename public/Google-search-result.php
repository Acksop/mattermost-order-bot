<?php

require dirname(dirname(__FILE__)). DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

define('PUBLIC_PATH', dirname(__FILE__));
define('APPLICATION_PATH', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "app");
define('PAGES_PATH', APPLICATION_PATH . DIRECTORY_SEPARATOR . "page");
define('LAYOUT_PATH', APPLICATION_PATH . DIRECTORY_SEPARATOR . "layout");

require APPLICATION_PATH . DIRECTORY_SEPARATOR . 'session.php';


//pour chaque page et mise en tampon
ob_start();
require_once PAGES_PATH . DIRECTORY_SEPARATOR . 'Gougou-search-results.php';
require_once PAGES_PATH . DIRECTORY_SEPARATOR . 'Gougou-search-results.phtml';
$content = ob_get_contents();
ob_end_clean();

require_once LAYOUT_PATH . DIRECTORY_SEPARATOR . $layout . '.phtml';