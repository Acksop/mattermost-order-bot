<?php

require dirname(dirname(__FILE__)). DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

define('PUBLIC_PATH', dirname(__FILE__));
define('APPLICATION_PATH', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "app");
define('CLASS_PATH', APPLICATION_PATH . DIRECTORY_SEPARATOR . "class");

require APPLICATION_PATH . DIRECTORY_SEPARATOR . "parameters.php";
require APPLICATION_PATH . DIRECTORY_SEPARATOR . 'session.php';

error_reporting(-1);
ini_set('display_errors', 1);

function passValide($password)
{

    //password_hash('tototo', HASH_ALGORITHM)

    if (password_verify($password, HASH)) {
        if (password_needs_rehash(HASH, HASH_ALGORITHM)) {
            $hash = password_hash($password, HASH_ALGORITHM);

            $file = "";
            $lines = file(APPLICATION_PATH . DIRECTORY_SEPARATOR . "parameters.php");
            foreach ($lines as $line_num => $line) {
                if (preg_match("#define\(HASH,(.*)\)#", $line, $matches)) {
                    $newline = preg_replace("#define\(HASH,(.*)\)#", "define(HASH,'$hash')", $line);
                } else {
                    $newline = $line;
                }
                if ($newline != "") {
                    $file .= $newline . "\n";
                }
            }
            /* Store new hash in parameters file */
            file_put_contents(APPLICATION_PATH . DIRECTORY_SEPARATOR . "parameters.php", $file);
        }
    }
    //try fallback passengers
    if (password_verify($password, HASH)) {
        return true;
    } else {
        return false;
    }
}

//https://code.tutsplus.com/tutorials/8-regular-expressions-you-should-know--net-6149

function extractLink($message)
{
    preg_match_all("#((?:https?:\/\/)?(?:[\da-z\.-]+)\.(?:[a-z\.]{2,6})(?:[\/\w \.-]*)*\/?)#", $message, $matches);
    return $matches;
}

function extractText($message)
{
    $links = extractLink($message);
    array_shift($links);
    foreach ($links as $link) {
        if ($link != "") {
            $message = preg_replace("#$link[0]#", " ... ", $message);
        }
    }
    return $message;
}

/**************************************************************************************************************/
/******************* TEST COTÉ SERVEUR POUR L'ENVOI DU MESSAGE    *********************************************/

$message = ForceUTF8\Encoding::fixUTF8(StringCompat::protectionDoubleQuote(StringCompat::protectionSimpleQuote($_POST['message'])));

if (passValide($_POST['password']) && $_POST['message'] != "") {

    $hookMattermost = "http://{your-mattermost-site}/hooks/xxx-generatedkey-xxx";

    $payload = <<<EOD
payload={
	"channel": "bot-tests",
	"username": "acksop",
	"text": "$message"
}
EOD;

    $json = <<<EOD
{
	"channel": "bot-tests",
	"username": "acksop",
	"text": "$message"
}
EOD;


    $commandURL = "curl -i -X POST --data-urlencode '$payload' $hookMattermost";
    $commandJSON = "curl -i -X POST -H 'Content-Type: application/json' -d '$json' $hookMattermost";

    echo "<pre>";
    print_r($commandJSON);
    echo "</pre><pre>";
    print_r($commandURL);

    $dom = new DOMDocument;
    $dom->load(PUBLIC_PATH . DIRECTORY_SEPARATOR . "veilleTinternet.xml");

    $noeudVeille = $dom->getElementsByTagName("veille")->item(0);

    $messageNode = $dom->createElement("message");

    $myOriginalTextNode = $dom->createElement("originel");
    $textNode = $dom->createTextNode(htmlentities($message));
    $myOriginalTextNode->appendChild($textNode);
    $messageNode->appendChild($myOriginalTextNode);

    $myTextNode = $dom->createElement("text");
    $textNode = $dom->createTextNode(htmlentities(extractText($message)));
    $myTextNode->appendChild($textNode);
    $messageNode->appendChild($myTextNode);

    $myLinksNode = $dom->createElement("links");
    $links = extractLink($message);
    array_shift($links);
    foreach ($links as $link) {
        if ($link != "") {
            $myLinkNode = $dom->createElement("link");
            $textNode = $dom->createTextNode(htmlentities($link[0]));
            $myLinkNode->appendChild($textNode);
            $myLinksNode->appendChild($myLinkNode);
        }
    }
    $messageNode->appendChild($myLinksNode);

    //$dom->insertBefore( $messageNode, $noeudVeille );
    $noeudVeille->appendChild($messageNode);

    $dom->save(PUBLIC_PATH . DIRECTORY_SEPARATOR . "veilleTinternet.xml");

    shell_exec($commandJSON);
    shell_exec($commandURL);
    die();
    if (isset($_POST['ajax'])) {
        echo "Votre Lien as &eacute;t&eacute; correctement transmis. Vous receverez une r&eacute;ponse dans les prochains jours.";
    } else {
        header("Location: /index.php?envoiDuMessage=oui");
    }

} else {
    if (isset($_POST['ajax'])) {
        echo "Votre Lien n'as pas &eacute;t&eacute; transmis. Veuillez v&eacute;rifiez les informations que vous avez saisies ...";
    } else {
        header("Location: /index.php?envoiDuMessage=non&message=" . StringCompat::myUrlEncode($message));
    }
}