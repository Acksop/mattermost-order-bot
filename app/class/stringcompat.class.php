<?php

class StringCompat
{
    public static function destructionH4x0RChaine($chaine)
    {
        return preg_replace('#[\x00-\x1F\x7F-\x9F/\\\\]#', '', $chaine);
    }

    public static function protectionDoubleQuote($chaine)
    {
        $chaine = preg_replace('/"(\w+)"/', '« ${1} »', $chaine);
        $chaine = preg_replace('#"#', '_', $chaine);
        return $chaine;
    }

    public static function protectionSimpleQuote($chaine)
    {
        $chaine = preg_replace("#'#", '_', $chaine);
        return $chaine;
    }

    public static function myUrlEncode($string)
    {
        $replacements = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D'/*, '%2B'*/, '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
        $entities = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "="/*, "+"*/, "$", ",", "/", "?", "%", "#", "[", "]");
        $string = urlencode($string);
        $string = str_replace($entities, $replacements, $string);
        return $string;
    }

    public static function myUrlDecode($string)
    {
        $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D'/*, '%2B'*/, '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
        $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "="/*, "+"*/, "$", ",", "/", "?", "%", "#", "[", "]");
        $string = str_replace($entities, $replacements, $string);
        $string = urldecode($string);
        return $string;

    }

}
